<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Product
{
    private $id;
    /**
     * @Assert\NotBlank(message="Please, upload the product brochure as a PDF file.")
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $brochure;

    public function getId()
    {
        return $this->id;
    }

    public function getBrochure()
    {
        return $this->brochure;
    }

    public function setBrochure($brochure): self
    {
        $this->brochure = $brochure;

        return $this;
    }
}
